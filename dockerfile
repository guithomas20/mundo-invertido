FROM httpd:latest
WORKDIR /usr/local/apache2/htdocs/
COPY assets/css /usr/local/apache2/htdocs/assets/css
COPY assets/images /usr/local/apache2/htdocs/assets/images
COPY assets/js /usr/local/apache2/htdocs/assets/js
COPY assets/musics /usr/local/apache2/htdocs/assets/musics
COPY index.html /usr/local/apache2/htdocs/

EXPOSE 80
